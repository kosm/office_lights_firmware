# Used C compliler
CC = avr-gcc

# Used object copy utility
OBJ_CP = avr-objcopy

# Used ar
AR = avr-ar

# The itility showing an ELF size
SIZE = avr-size
    
# Optimization level
OPT_LEVEL = s

# Used C standard
C_STD = c99

# Warning flags
WFLAGS = -Werror

CFLAGS = -mmcu=$(MCU) -DF_CPU=$(F_CPU) -DF_CLOCK=$(F_CLOCK) -DBOARD=BOARD_$(BOARD) \
    -O$(OPT_LEVEL) -std=$(C_STD) $(WFLAGS) \
    -D USB_DEVICE_ONLY -D USE_FLASH_DESCRIPTORS -D FIXED_CONTROL_ENDPOINT_SIZE=8 -D FIXED_NUM_CONFIGURATIONS=1 \
    -D TIMER_DEBUG
#    -D USE_STATIC_OPTIONS="(USB_OPT_REG_ENABLED | USB_OPT_AUTO_PLL)" \
#   -D NO_STREAM_CALLBACKS

OBJDIR = .

#all.subs:
#	@for dir in $(SUBDIRS); do \
#	    $(MAKE) -C $${dir} all; \
#	done;

#clean.subs:
#	@for dir in $(SUBDIRS); do \
#	    $(MAKE) -C $${dir} clean; \
#	done;
