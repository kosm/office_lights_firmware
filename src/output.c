/*
 * output.c
 *
 *  Created on: 09.01.2011
 *      Author: kosm
 */

#include <stddef.h>

#include <output.h>
#include <scheduler.h>
#include <hardware/stled.h>
#include <hardware/generic.h>

#define MESSAGE_BUFFER_SIZE 64
#define INDICATE_0_DELAY 1
#define INDICATE_1_DELAY 5
#define INTERBIT_GAP_DELAY 4
#define INTERSTATUS_GAP_DELAY 20

#define HALT_INDICATION_DELAY 30

#define INDICATION_TASK_PERIOD 499

static const uint8_t normal_indication_delays[] = { 1, 100 };
static const StatusIndicationScript normal_indication_script = { (uint8_t *)normal_indication_delays,
		sizeof(normal_indication_delays), true };
static const uint8_t error_indication_delays[] = { 5, 5 };
static const StatusIndicationScript error_indication_script = { (uint8_t *)error_indication_delays,
		sizeof(normal_indication_delays), true };
static const uint8_t no_out_power_indication_delays[] = { 100, 1 };
static const StatusIndicationScript no_out_power_indication_script = { (uint8_t *)no_out_power_indication_delays,
		sizeof(no_out_power_indication_delays), true };

static const Task status_indication_task = { output_indication_routine, NULL, INDICATION_TASK_PERIOD };

static volatile bool initialized = false;
static const StatusIndicationScript *current_indication_script;
static uint8_t indication_phase;
static uint8_t indication_delay;

/* This function should be executed 10 times per second */
static void toggle_stled(uint8_t status_code) {
	static uint8_t current_status_code = SC_NONE;
	static uint8_t counter = 0, code_len = 0, bit = 0;

	if (status_code != current_status_code) {
		current_status_code = status_code;
		counter = 0;
		for (code_len = 8; code_len > 0 && (status_code ^ (1 << (code_len - 1))); --code_len);
		bit = code_len - 1;
		stled_set_state(true);
	}
	if (current_status_code == SC_NONE) return;

	if (stled_get_state()) {
		if (((status_code & (1 << bit)) && counter < INDICATE_1_DELAY) ||
				((status_code ^ (1 << bit)) && counter < INDICATE_0_DELAY)) ++counter;
		else {
			stled_set_state(false);
			counter = 0;
			--bit;
		}
	}
	else if (bit < 0) {
		if (counter < INTERSTATUS_GAP_DELAY) ++counter;
		else {
			counter = 0;
			bit = code_len - 1;
			stled_set_state(true);
		}
	}
	else {
		if (counter < INTERBIT_GAP_DELAY) ++counter;
		else {
			counter = 0;
			stled_set_state(true);
		}
	}
}

/*void output_print(OutputLevel output_level, const char *format, ...) {
	char message_buffer[MESSAGE_BUFFER_SIZE];
	return;
	va_list varg;

	va_start(varg, format);
	vsnprintf(message_buffer, sizeof(message_buffer), format, varg);
	va_end(varg);
}*/

void output_status_and_halt(uint8_t status_code) {
	while (1) {
		toggle_stled(status_code);
		delay(HALT_INDICATION_DELAY);
	}
}

void output_status(uint8_t status_code) {
	const StatusIndicationScript *status_script;

	switch (status_code) {
		case SC_NORMAL:
			status_script = &normal_indication_script;
			break;
		case SC_NONE:
			status_script = NULL;
			break;
		case SC_NO_OUTPUT_POWER:
			status_script = &no_out_power_indication_script;
			break;
		case SC_EXTRA_TASK:
		case SC_INVALID_VALUE:
		default:
			status_script = &error_indication_script;
	}

	output_indicate_status(status_script);
}

void output_indication_routine(void *argument) {
	//toggle_stled(current_status);
	if (current_indication_script == NULL) return;

	if (indication_delay >= current_indication_script->delays[indication_phase]) {
		indication_delay = 0;
		++indication_phase;
		stled_toggle();
	}
	else ++indication_delay;

	if (indication_phase >= current_indication_script->length) {
		indication_phase = 0;
		stled_set_state(current_indication_script->initial_state);
	}
}

void output_indicate_status(const StatusIndicationScript *indication_script) {
	if (indication_script == NULL) {
		stled_set_state(false);
		return;
	}
	current_indication_script = indication_script;
	indication_phase = indication_delay = 0;
	stled_set_state(indication_script->initial_state);
}

void output_init() {
	if (initialized) return;
	current_indication_script = NULL;
	indication_phase = indication_delay = 0;
	scheduler_add_task(&status_indication_task);
	initialized = true;
}
