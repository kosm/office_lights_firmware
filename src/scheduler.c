/*
 * scheduler.c
 *
 *  Created on: 13.02.2011
 *      Author: kosm
 */

#include <stdbool.h>
#include <string.h>

#include <scheduler.h>
#include <output.h>
#include <hardware/timer.h>

#define MAX_TASK_NUMBER 10

typedef struct {
	const Task *task;
	uint32_t delay;
} TaskContext;

static volatile TaskContext task_list[MAX_TASK_NUMBER];
static volatile uint8_t task_count;
static volatile bool initialized = false;

static void scheduler_process_tasks() {
	uint8_t task_num;

	for (task_num = 0; task_num < task_count; ++task_num) {
		if (task_list[task_num].delay == task_list[task_num].task->period) {
			task_list[task_num].task->routine(task_list[task_num].task->argument);
			task_list[task_num].delay = 0;
		}
		else ++task_list[task_num].delay;
	}
}

void scheduler_add_task(const Task *task) {
	if (task_count == MAX_TASK_NUMBER) {
		output_status(SC_EXTRA_TASK);
		return;
	}
	memset((void *)&task_list[task_count], 0, sizeof(TaskContext));
	task_list[task_count++].task = task;
}

void scheduler_remove_task(const Task *task) {
	uint8_t task_num;

	for (task_num = 0; task_num < task_count; ++task_num) {
		if (task_list[task_num].task == task) {
			memmove((void *)&task_list[task_num], (void *)&task_list[task_num + 1], task_count - task_num);
			--task_count;
#ifdef REMOVE_FIRST_TASK_ONLY
			break;
#endif
		}
	}
}

void scheduler_init() {
	if (initialized) return;
	task_count = 0;
	timer_init();
	timer_set_compare_handler(scheduler_process_tasks);
	initialized = true;
}
