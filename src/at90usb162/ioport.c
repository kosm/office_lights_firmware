/*
 * ioport.c
 *
 *  Created on: 09.01.2011
 *      Author: kosm
 */

#include <hardware/ioport.h>
#include <output.h>

#include <avr/io.h>

static void unsupported_port_error(const IOPortPin *pin) {
	/*output_print(OUTPUT_ERROR, PSTR("Unsupported port number: %c\n"), 'A' + pin->port_number);*/
}

static void invalid_direction_error(const IOPortPin *pin) {
	/*output_print(OUTPUT_ERROR, PSTR("The pin %d of the PORT%c has an invalid direction\n"),
			pin->pin_number, 'A' + pin->port_number);*/
}

void ioport_init() {
	/* Nothing to do */
}

void ioport_configure_pin(const IOPortPinConfig *pin_config) {
	switch (pin_config->port_pin.port_number) {
		case B:
			if (pin_config->pin_direction == PD_INPUT) DDRB &= ~_BV(pin_config->port_pin.pin_number);
			else DDRB |= _BV(pin_config->port_pin.pin_number);
			if (pin_config->initial_state) PORTB |= _BV(pin_config->port_pin.pin_number);
			else PORTB &= ~_BV(pin_config->port_pin.pin_number);
			break;
		case C:
			if (pin_config->pin_direction == PD_INPUT) DDRC &= ~_BV(pin_config->port_pin.pin_number);
			else DDRC |= _BV(pin_config->port_pin.pin_number);
			if (pin_config->initial_state) PORTC |= _BV(pin_config->port_pin.pin_number);
			else PORTC &= ~_BV(pin_config->port_pin.pin_number);
			break;
		case D:
			if (pin_config->pin_direction == PD_INPUT) DDRD &= ~_BV(pin_config->port_pin.pin_number);
			else DDRD |= _BV(pin_config->port_pin.pin_number);
			if (pin_config->initial_state) PORTD |= _BV(pin_config->port_pin.pin_number);
			else PORTD &= ~_BV(pin_config->port_pin.pin_number);
			break;
		default:
			unsupported_port_error(&pin_config->port_pin);
	}
}

void ioport_configure_pins(const IOPortPinConfig pin_configs[], uint8_t length) {
	uint8_t config_number;

	for (config_number = 0; config_number < length; ++config_number)
		ioport_configure_pin(&pin_configs[config_number]);
}

void ioport_set_pin(const IOPortPin *pin, bool value) {
	switch (pin->port_number) {
		case B:
			if (DDRB & _BV(pin->pin_number)) {
				if (value) PORTB |= _BV(pin->pin_number);
				else PORTB &= ~_BV(pin->pin_number);
			}
			else invalid_direction_error(pin);
			break;
		case C:
			if (DDRC & _BV(pin->pin_number)) {
				if (value) PORTC |= _BV(pin->pin_number);
				else PORTC &= ~_BV(pin->pin_number);
			}
			else invalid_direction_error(pin);
			break;
		case D:
			if (DDRD & _BV(pin->pin_number)) {
				if (value) PORTD |= _BV(pin->pin_number);
				else PORTD &= ~_BV(pin->pin_number);
			}
			else invalid_direction_error(pin);
			break;
		default:
			unsupported_port_error(pin);
	}
}

bool ioport_get_pin(const IOPortPin *pin) {
	switch (pin->port_number) {
		case B:
			return PINB & _BV(pin->pin_number) ? true : false;
			break;
		case C:
			return PINC & _BV(pin->pin_number) ? true : false;
			break;
		case D:
			return PIND & _BV(pin->pin_number) ? true : false;
			break;
		default:
			unsupported_port_error(pin);
	}
	return false;
}

