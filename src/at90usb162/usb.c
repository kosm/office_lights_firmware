/*
 * usb.c
 *
 *  Created on: 07.01.2011
 *      Author: kosm
 */

#include <avr/io.h>

#include <LUFA/Version.h>
#include <LUFA/Drivers/USB/USB.h>

#include <common.h>
#include <scheduler.h>
#include <olcp_ll.h>

#include <hardware/stled.h>
#include <hardware/ioport.h>
#include <hardware/usb.h>

#define USB_TASK_PERIOD 200
#define MANUFACTURER_STR L"KOSM & Zhemoldi"
#define PRODUCT_STR L"Office Lights"

static volatile bool initialized = false;
static IOPortPinConfig ch1_pin_config = { { B, 4 }, PD_OUTPUT, false };
static const Task usb_task = { usb_routine, NULL, USB_TASK_PERIOD };

typedef struct
{
	USB_Descriptor_Configuration_Header_t Config;
	USB_Descriptor_Interface_t            HID_Interface;
	USB_HID_Descriptor_HID_t              HID_GenericHID;
	USB_Descriptor_Endpoint_t             HID_ReportINEndpoint;
} USB_Descriptor_Configuration_t;

/** Endpoint number of the Generic HID reporting IN endpoint. */
#define GENERIC_IN_EPNUM          1
/** Size in bytes of the Generic HID reporting endpoint. */
#define GENERIC_EPSIZE            8
/** Size in bytes of the Generic HID reports (including report ID byte). */
#define GENERIC_REPORT_SIZE       8

USB_Descriptor_HIDReport_Datatype_t PROGMEM GenericReport[] =
{
	0x06, 0x9c, 0xff,     /* Usage Page (Vendor Defined)                     */
	0x09, 0x01,           /* Usage (Vendor Defined)                          */
	0xa1, 0x01,           /* Collection (Vendor Defined)                     */
	0x09, 0x02,           /*   Usage (Vendor Defined)                        */
	0x75, 0x08,           /*   Report Size (8)                               */
	0x95, GENERIC_REPORT_SIZE, /*   Report Count (GENERIC_REPORT_SIZE)       */
	0x15, 0x80,           /*   Logical Minimum (-128)                        */
	0x25, 0x7F,           /*   Logical Maximum (127)                         */
	0x81, 0x02,           /*   Input (Data, Variable, Absolute)              */
	0x09, 0x03,           /*   Usage (Vendor Defined)                        */
	0x75, 0x08,           /*   Report Size (8)                               */
	0x95, GENERIC_REPORT_SIZE, /*   Report Count (GENERIC_REPORT_SIZE)       */
	0x15, 0x00,           /*   Logical Minimum (0)                           */
	0x25, 0xff,           /*   Logical Maximum (255)                         */
	0x91, 0x02,           /*   Output (Data, Variable, Absolute)             */
	0xc0                  /* End Collection                                  */
};

USB_Descriptor_Device_t PROGMEM DeviceDescriptor =
{
	.Header                 = {.Size = sizeof(USB_Descriptor_Device_t), .Type = DTYPE_Device},
	.USBSpecification       = VERSION_BCD(01.10),
	.Class                  = USB_CSCP_NoDeviceClass,
	.SubClass               = USB_CSCP_NoDeviceSubclass,
	.Protocol               = USB_CSCP_NoDeviceProtocol,
	.Endpoint0Size          = FIXED_CONTROL_ENDPOINT_SIZE,
	.VendorID               = 0x03EB,
	.ProductID              = 0x206F,
	.ReleaseNumber          = VERSION_BCD(00.01),
	.ManufacturerStrIndex   = 0x01,
	.ProductStrIndex        = 0x02,
	.SerialNumStrIndex      = NO_DESCRIPTOR,
	.NumberOfConfigurations = FIXED_NUM_CONFIGURATIONS
};

/** Configuration descriptor structure. This descriptor, located in FLASH memory, describes the usage
 *  of the device in one of its supported configurations, including information about any device interfaces
 *  and endpoints. The descriptor is read out by the USB host during the enumeration process when selecting
 *  a configuration so that the host may correctly communicate with the USB device.
 */
USB_Descriptor_Configuration_t PROGMEM ConfigurationDescriptor =
{
	.Config =
		{
			.Header                 = {.Size = sizeof(USB_Descriptor_Configuration_Header_t), .Type = DTYPE_Configuration},
			.TotalConfigurationSize = sizeof(USB_Descriptor_Configuration_t),
			.TotalInterfaces        = 1,
			.ConfigurationNumber    = 1,
			.ConfigurationStrIndex  = NO_DESCRIPTOR,
			.ConfigAttributes       = (USB_CONFIG_ATTR_BUSPOWERED | USB_CONFIG_ATTR_SELFPOWERED),
			.MaxPowerConsumption    = USB_CONFIG_POWER_MA(100)
		},

	.HID_Interface =
		{
			.Header                 = {.Size = sizeof(USB_Descriptor_Interface_t), .Type = DTYPE_Interface},
			.InterfaceNumber        = 0x00,
			.AlternateSetting       = 0x00,
			.TotalEndpoints         = 1,
			.Class                  = HID_CSCP_HIDClass,
			.SubClass               = HID_CSCP_NonBootSubclass,
			.Protocol               = HID_CSCP_NonBootProtocol,
			.InterfaceStrIndex      = NO_DESCRIPTOR
		},

	.HID_GenericHID =
		{
			.Header                 = {.Size = sizeof(USB_HID_Descriptor_HID_t), .Type = HID_DTYPE_HID},
			.HIDSpec                = VERSION_BCD(01.11),
			.CountryCode            = 0x00,
			.TotalReportDescriptors = 1,
			.HIDReportType          = HID_DTYPE_Report,
			.HIDReportLength        = sizeof(GenericReport)
		},

	.HID_ReportINEndpoint =
		{
			.Header                 = {.Size = sizeof(USB_Descriptor_Endpoint_t), .Type = DTYPE_Endpoint},
			.EndpointAddress        = (ENDPOINT_DESCRIPTOR_DIR_IN | GENERIC_IN_EPNUM),
			.Attributes             = (EP_TYPE_INTERRUPT | ENDPOINT_ATTR_NO_SYNC | ENDPOINT_USAGE_DATA),
			.EndpointSize           = GENERIC_EPSIZE,
			.PollingIntervalMS      = 0x01
		},
};

/** Language descriptor structure. This descriptor, located in FLASH memory, is returned when the host requests
 *  the string descriptor with index 0 (the first index). It is actually an array of 16-bit integers, which indicate
 *  via the language ID table available at USB.org what languages the device supports for its string descriptors.
 */
USB_Descriptor_String_t PROGMEM LanguageString =
{
	.Header                 = {.Size = USB_STRING_LEN(1), .Type = DTYPE_String},
	.UnicodeString          = {LANGUAGE_ID_ENG}
};

/** Manufacturer descriptor string. This is a Unicode string containing the manufacturer's details in human readable
 *  form, and is read out upon request by the host when the appropriate string ID is requested, listed in the Device
 *  Descriptor.
 */
USB_Descriptor_String_t PROGMEM ManufacturerString =
{
	.Header                 = {.Size = USB_STRING_LEN(15), .Type = DTYPE_String},
	.UnicodeString          = MANUFACTURER_STR
};

/** Product descriptor string. This is a Unicode string containing the product's details in human readable form,
 *  and is read out upon request by the host when the appropriate string ID is requested, listed in the Device
 *  Descriptor.
 */
USB_Descriptor_String_t PROGMEM ProductString =
{
	.Header                 = {.Size = USB_STRING_LEN(13), .Type = DTYPE_String},

	.UnicodeString          = PRODUCT_STR
};

/** This function is called by the library when in device mode, and must be overridden (see library "USB Descriptors"
 *  documentation) by the application code so that the address and size of a requested descriptor can be given
 *  to the USB library. When the device receives a Get Descriptor request on the control endpoint, this function
 *  is called so that the descriptor details can be passed back and the appropriate descriptor sent back to the
 *  USB host.
 */
uint16_t CALLBACK_USB_GetDescriptor(const uint16_t wValue,
                                    const uint8_t wIndex,
                                    const void** const DescriptorAddress)
{
	const uint8_t  DescriptorType   = (wValue >> 8);
	const uint8_t  DescriptorNumber = (wValue & 0xFF);

	const void* Address = NULL;
	uint16_t    Size    = NO_DESCRIPTOR;

	switch (DescriptorType)
	{
		case DTYPE_Device:
			Address = &DeviceDescriptor;
			Size    = sizeof(USB_Descriptor_Device_t);
			break;
		case DTYPE_Configuration:
			Address = &ConfigurationDescriptor;
			Size    = sizeof(USB_Descriptor_Configuration_t);
			break;
		case DTYPE_String:
			switch (DescriptorNumber)
			{
				case 0x00:
					Address = &LanguageString;
					Size    = pgm_read_byte(&LanguageString.Header.Size);
					break;
				case 0x01:
					Address = &ManufacturerString;
					Size    = pgm_read_byte(&ManufacturerString.Header.Size);
					break;
				case 0x02:
					Address = &ProductString;
					Size    = pgm_read_byte(&ProductString.Header.Size);
					break;
			}

			break;
		case HID_DTYPE_HID:
			Address = &ConfigurationDescriptor.HID_GenericHID;
			Size    = sizeof(USB_HID_Descriptor_HID_t);
			break;
		case HID_DTYPE_Report:
			Address = &GenericReport;
			Size    = sizeof(GenericReport);
			break;
	}

	*DescriptorAddress = Address;
	return Size;
}

/** Buffer to hold the previously generated HID report, for comparison purposes inside the HID class driver. */
uint8_t PrevHIDReportBuffer[GENERIC_REPORT_SIZE];

static USB_ClassInfo_HID_Device_t generic_hid_interface =
{
	.Config =
		{
			.InterfaceNumber              = 0,
			.ReportINEndpointNumber       = GENERIC_IN_EPNUM,
			.ReportINEndpointSize         = GENERIC_EPSIZE,
			.ReportINEndpointDoubleBank   = false,
			.PrevReportINBuffer           = PrevHIDReportBuffer,
			.PrevReportINBufferSize       = sizeof(PrevHIDReportBuffer),
		},
};

/** Event handler for the library USB Connection event. */
void EVENT_USB_Device_Connect(void)
{
	// LEDs_SetAllLEDs(LEDMASK_USB_ENUMERATING);
}

/** Event handler for the library USB Disconnection event. */
void EVENT_USB_Device_Disconnect(void)
{
	// LEDs_SetAllLEDs(LEDMASK_USB_NOTREADY);
}

/** Event handler for the library USB Configuration Changed event. */
void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;
	ConfigSuccess &= HID_Device_ConfigureEndpoints(&generic_hid_interface);
	USB_Device_EnableSOFEvents();
	// LEDs_SetAllLEDs(ConfigSuccess ? LEDMASK_USB_READY : LEDMASK_USB_ERROR);
}

/** Event handler for the library USB Control Request reception event. */
void EVENT_USB_Device_ControlRequest(void)
{
	HID_Device_ProcessControlRequest(&generic_hid_interface);
}

/** Event handler for the USB device Start Of Frame event. */
void EVENT_USB_Device_StartOfFrame(void)
{
	HID_Device_MillisecondElapsed(&generic_hid_interface);
}

bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                         uint8_t* const ReportID,
                                         const uint8_t ReportType,
                                         void* ReportData,
                                         uint16_t* const ReportSize)
{
	uint8_t olcp_resp_size;
	const uint8_t *olcp_resp_buffer = olcp_get_response_buffer(&olcp_resp_size);
	*ReportSize = (uint16_t)olcp_resp_size;
	memcpy(ReportData, olcp_resp_buffer, olcp_resp_size);

	return true;
}

void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t* const HIDInterfaceInfo,
                                          const uint8_t ReportID,
                                          const uint8_t ReportType,
                                          const void* ReportData,
                                          const uint16_t ReportSize)
{
	olcp_process_request(ReportData, (uint8_t)ReportSize);
}

/* Initializes USB. PLL must be configured before executing this function! */
void usb_init() {
	if (initialized) return;
	ioport_configure_pin(&ch1_pin_config);
	USB_Init(USB_OPT_MANUAL_PLL | USB_OPT_REG_ENABLED | USB_DEVICE_OPT_FULLSPEED);
	scheduler_add_task(&usb_task);
	initialized = true;
}

/* The function continuously processing USB events */
void usb_routine(void *argument) {
	USB_USBTask();
	HID_Device_USBTask(&generic_hid_interface);
}
