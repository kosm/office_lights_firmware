/*
 * stled.c
 *
 *  Created on: 10.01.2011
 *      Author: kosm
 */

#include <hardware/stled.h>
#include <hardware/ioport.h>

static volatile bool initialized = false;
static const IOPortPinConfig stled_pin_config = { { B, 7 }, PD_OUTPUT, false };
static bool current_state = false;

/* Initializes Status LED */
void stled_init() {
	if (initialized) return;
	ioport_configure_pin(&stled_pin_config);
	initialized = true;
}

void stled_set_state(bool state) {
	ioport_set_pin(&stled_pin_config.port_pin, state);
	current_state = state;
}

bool stled_get_state() {
	return current_state;
}

void stled_toggle() {
	stled_set_state(!current_state);
}
