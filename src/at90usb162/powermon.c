/*
 * powermon.c
 *
 *  Created on: 08.03.2011
 *      Author: kosm
 */

#include <stddef.h>

#include <hardware/ioport.h>
#include <scheduler.h>
#include <output.h>

#define POWERMON_TASK_PERIOD 4999

static const IOPortPinConfig powermon_port_config = { { B, 6 }, PD_INPUT, true };
static volatile bool recent_state;

static void powermon_routine(void *argument) {
	if (ioport_get_pin(&powermon_port_config.port_pin) && recent_state) {
		output_status(SC_NO_OUTPUT_POWER);
		recent_state = !recent_state;
	}
	else if (!ioport_get_pin(&powermon_port_config.port_pin) && !recent_state) {
		output_status(SC_NORMAL);
		recent_state = !recent_state;
	}
}

static const Task powermon_task = { powermon_routine, NULL, POWERMON_TASK_PERIOD };

static volatile bool initialized = false;

void powermon_init() {
	if (!initialized) {
		ioport_configure_pin(&powermon_port_config);
		scheduler_add_task(&powermon_task);
		recent_state = true;
		initialized = true;
	}
}

