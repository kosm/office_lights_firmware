/*
 * timer.c
 *
 *  Created on: 10.02.2011
 *      Author: kosm
 */

#include <stdbool.h>
#include <stddef.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include <hardware/timer.h>
#include <hardware/ioport.h>

/* The timer resolution defining the rate of system events */
#define TIMER_RESOLUTION 99

static volatile bool initialized = false;
static volatile CounterCompareHandler compare_handler;

#ifdef TIMER_DEBUG
static const IOPortPinConfig timer_debug_pin_config = { { C, 5 }, PD_OUTPUT, false };
#endif

/* The vector handling the timer compare event */
ISR(TIMER0_COMPA_vect) {
	if (compare_handler != NULL) compare_handler();
#ifdef TIMER_DEBUG
	PORTC ^= _BV(PIN5);
#endif
}

void timer_set_compare_handler(CounterCompareHandler handler) {
	compare_handler = handler;
}

/* Initializes the timer/counter 0 */
void timer_init() {

	if (initialized) return;
	compare_handler = NULL;

	OCR0A = TIMER_RESOLUTION;

	/* Setting the CTC timer mode */
	TCCR0A = _BV(WGM01);
	/* Unmasking the counter match interrupt */
	TIMSK0 |= _BV(OCIE0A);
	/* Dividing the system clock by 8, getting finally 1 MHz */
	TCCR0B = _BV(CS01);
	/* 8 MHz / 8 / 100 = 10 KHz */
	/* So we have finally 10K events per second */

#ifdef TIMER_DEBUG
	ioport_configure_pin(&timer_debug_pin_config);
#endif

	initialized = true;
}
