/*
 * channels.c
 *
 *  Created on: 27.02.2011
 *      Author: kosm
 */

#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include <scheduler.h>
#include <hardware/ioport.h>
#include <hardware/channels.h>

#define MAX_CHANNEL_BRIGHTNESS 32
#define BRIGNESS_TASK_PERIOD 2
#define BLINKING_TASK_PERIOD 999

typedef struct {
	bool state;
	bool blinking;
	uint8_t brightness;
	uint8_t blinking_period;
	uint8_t brightness_delay;
	uint8_t blinking_delay;
} ChannelParameters;

static const IOPortPinConfig channel_port_config[] = {
		{ { C, 6 }, PD_OUTPUT, false },
		{ { B, 4 }, PD_OUTPUT, false },
		{ { B, 5 }, PD_OUTPUT, false },
		{ { C, 2 }, PD_OUTPUT, false },
		{ { D, 4 }, PD_OUTPUT, false },
		{ { D, 0 }, PD_OUTPUT, false },
		{ { D, 5 }, PD_OUTPUT, false },
};

static ChannelParameters channel_parameters[CHANNEL_NUMBER];
static const Task channels_brightness_task = { channels_brightness_routine, NULL, BRIGNESS_TASK_PERIOD };
static const Task channels_blinking_task = { channels_blinking_routine, NULL, BLINKING_TASK_PERIOD };

static volatile bool initialized = false;

void channels_brightness_routine(void *argument) {
	uint8_t ch_num;
	for (ch_num = 0; ch_num < CHANNEL_NUMBER; ++ch_num) {
		if (channel_parameters[ch_num].state) {
			if (channel_parameters[ch_num].brightness_delay == channel_parameters[ch_num].brightness) {
				ioport_set_pin(&channel_port_config[ch_num].port_pin, false);
				++channel_parameters[ch_num].brightness_delay;
			}
			else if (channel_parameters[ch_num].brightness_delay >= MAX_CHANNEL_BRIGHTNESS) {
				ioport_set_pin(&channel_port_config[ch_num].port_pin, true);
				channel_parameters[ch_num].brightness_delay = 0;
			}
			else ++channel_parameters[ch_num].brightness_delay;
		}
	}
}

void channels_blinking_routine(void *argument) {
	uint8_t ch_num;
	for (ch_num = 0; ch_num < CHANNEL_NUMBER; ++ch_num) {
		if (channel_parameters[ch_num].blinking) {
			if (channel_parameters[ch_num].blinking_delay >= channel_parameters[ch_num].blinking_period) {
				channels_set_state(ch_num, !channel_parameters[ch_num].state);
				channel_parameters[ch_num].blinking_delay = 0;
			}
			else ++channel_parameters[ch_num].blinking_delay;
		}
	}
}

void channels_init() {
	if (initialized) return;
	memset(channel_parameters, 0, sizeof(ChannelParameters) * CHANNEL_NUMBER);
	ioport_configure_pins(channel_port_config, CHANNEL_NUMBER);
	scheduler_add_task(&channels_brightness_task);
	scheduler_add_task(&channels_blinking_task);
	initialized = true;
}

void channels_set_state(uint8_t channel_num, bool state) {
	if (channel_parameters[channel_num].state != state) {
		channel_parameters[channel_num].state = state;
		ioport_set_pin(&channel_port_config[channel_num].port_pin, state);
	}
}

void channels_set_state_all(bool state) {
	uint8_t channel_num;
	for (channel_num = 0; channel_num < CHANNEL_NUMBER; ++channel_num)
		channels_set_state(channel_num, state);
}

void channels_set_blinking(uint8_t channel_num, bool blinking) {
	if (channel_parameters[channel_num].blinking != blinking) {
		channel_parameters[channel_num].blinking = blinking;
	}
}

void channels_set_blinking_all(bool blinking) {
	uint8_t channel_num;
	for (channel_num = 0; channel_num < CHANNEL_NUMBER; ++channel_num)
		channels_set_blinking(channel_num, blinking);
}

void channels_set_brightness(uint8_t channel_num, uint8_t brightness) {
	if (channel_parameters[channel_num].brightness != brightness) {
		channel_parameters[channel_num].brightness = brightness;
	}
}

void channels_set_brightness_all(uint8_t brightness) {
	uint8_t channel_num;
	for (channel_num = 0; channel_num < CHANNEL_NUMBER; ++channel_num)
		channels_set_brightness(channel_num, brightness);
}

void channels_set_blinking_period(uint8_t channel_num, uint8_t period) {
	if (channel_parameters[channel_num].blinking_period != period) {
		channel_parameters[channel_num].blinking_period = period;
	}
}

void channels_set_blinking_period_all(uint8_t period) {
	uint8_t channel_num;
	for (channel_num = 0; channel_num < CHANNEL_NUMBER; ++channel_num)
		channels_set_blinking_period(channel_num, period);
}
