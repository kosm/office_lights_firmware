/*
 * generic.c
 *
 *  Created on: 05.01.2011
 *      Author: kosm
 */

#include <hardware/generic.h>
#include <common.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>

static void kill_wdt();
static void clock_setup();
static void sleep_setup();

static volatile bool initialized = false;

/* Disables a watchdog timer */
static void kill_wdt() {
	cli();
	wdt_reset();
	/* Prohibiting reset by WD */
	MCUSR &= ~_BV(WDRF);
	/* Preparing WD for its death... */
	WDTCSR |= _BV(WDCE) | _BV(WDE);
	/* ... and killing them! */
	WDTCSR = 0x00;
	/* Victory! Let there be interrupts */
	sei();
}

/* Configures clocks */
static void clock_setup() {
	/* Street Magic with USB */
	/*UDINT &= ~_BV(WAKEUPI);*/
	USBCON |= _BV(FRZCLK);
	/* Configuring a crystal oscillator */
	CKSEL1 |= _BV(EXCKSEL3) | _BV(EXCKSEL2) | _BV(EXCKSEL1) | _BV(EXCKSEL0);
	CKSEL0 |= _BV(EXSUT0);  /* BOD */
	CKSEL0 &= ~_BV(EXSUT1); /*     */
	/* Enabling the oscillator */
	CKSEL0 |= _BV(EXTE);
	/* Waiting for the oscillator to stabilize */
	loop_until_bit_is_set(CKSTA, EXTON);
	/* The oscillator is ready - selecting them as a primary clock source */
	CKSEL0 |= _BV(CLKS);
	/* Disabling RC clock */
	CKSEL0 &= ~_BV(RCE);
	/* Preparing clock prescaler to change a divisor */
	CLKPR = _BV(CLKPCE);
	/* Changing divisor to 2x */
	CLKPR = _BV(CLKPS0);
	//CLKPR &= ~(_BV(CLKPS1) | _BV(CLKPS2) | _BV(CLKPS3) | _BV(CLKPS0));
	/* Setting up PLL division factor to 2x */
	PLLCSR |= _BV(PLLP0);
	PLLCSR &= ~_BV(PLLP1);
	PLLCSR &= ~_BV(PLLP2);
	/* Enabling PLL */
	PLLCSR |= _BV(PLLE);
	/* Waiting for PLL */
	loop_until_bit_is_set(PLLCSR, PLOCK);
	/* Unfreezing USB clock */
	USBCON &= ~_BV(FRZCLK);
}

/* Configures MCU sleep mode */
static void sleep_setup() {
	set_sleep_mode(SLEEP_MODE_IDLE);
}

void generic_init() {
	if (initialized) return;
	/* Killing watchdog */
	kill_wdt();
	clock_setup();
	sleep_setup();
	initialized = true;
}

/* Entering into the sleep mode */
void sleep() {
	sleep_mode();
}

void delay(uint16_t interval) {
	for (; interval > 0; --interval);
}
