/*
 * olcp_ll.c
 *
 *  Created on: 28.02.2011
 *      Author: kosm
 */

#include <stddef.h>

#include <olcp_ll.h>
#include <hardware/channels.h>
#include <olcp.h>
#include <olcp_low_level.h>

static uint8_t response_buffer[MAX_OLCP_REPORT_SIZE];
static uint8_t response_size;

static void olcp_build_ack() {
	BasicReport *report = (BasicReport *)response_buffer;

	report->report_class = OLCP_RC_BASIC;
	report->report_code = OLCP_BS_ACK;

	response_size = sizeof(BasicReport);
}

static void olcp_build_error_report(uint8_t report_class, uint8_t error_reason) {
	BasicErrorResponse *report = (BasicErrorResponse *)response_buffer;

	report->head.report_class = report_class;
	report->head.report_code = OLCP_BS_ERROR;
	report->reason = error_reason;

	response_size = sizeof(BasicErrorResponse);
}

static void olcp_process_basic(const BasicReport *request) {

}

static void olcp_process_low_level(const void *request, uint8_t size) {
	const LowLevelReportHead *head = (LowLevelReportHead *)request;
	const LowLevelReportParam *params = (LowLevelReportParam *)((const uint8_t *)request + sizeof(LowLevelReportHead));
	const uint8_t param_count = (size - sizeof(LowLevelReportHead)) / sizeof(LowLevelReportParam);
	uint8_t channel_num, param_num;

	switch (head->entity) {
		case OLCP_LLE_CHANNEL1:
		case OLCP_LLE_CHANNEL2:
		case OLCP_LLE_CHANNEL3:
		case OLCP_LLE_CHANNEL4:
		case OLCP_LLE_CHANNEL5:
		case OLCP_LLE_CHANNEL6:
		case OLCP_LLE_CHANNEL7:
			channel_num = head->entity - OLCP_LLE_CHANNEL1;
			break;
		default:
			olcp_build_error_report(OLCP_RC_LOW_LEVEL, OLCP_LLER_UNKNOWN_ENTITY);
			return;
	}

	for (param_num = 0; param_num < param_count; ++param_num) {
		switch (params[param_num].parameter) {
			case OLCP_LLP_CHANNEL_STATE:
				channels_set_state(channel_num, params[param_num].value);
				break;
			case OLCP_LLP_CHANNEL_BRIGHTNESS:
				channels_set_brightness(channel_num, params[param_num].value);
				break;
			case OLCP_LLP_CHANNEL_BLINKING:
				channels_set_blinking(channel_num, params[param_num].value);
				break;
			case OLCP_LLP_CHANNEL_BLINKING_PERIOD:
				channels_set_blinking_period(channel_num, params[param_num].value);
				break;
			default:
				olcp_build_error_report(OLCP_RC_LOW_LEVEL, OLCP_LLER_UNKNOWN_PARAM);
				return;
		}
	}

	olcp_build_ack();
}

uint8_t *olcp_get_response_buffer(uint8_t *buffer_size) {
	if (buffer_size != NULL) *buffer_size = response_size;
	return response_buffer;
}

void olcp_process_request(const void *request, uint8_t size) {
	const BasicReport *report_head = (BasicReport *)request;

	switch (report_head->report_class) {
		case OLCP_RC_BASIC:
			olcp_process_basic(report_head);
			break;
		case OLCP_RC_LOW_LEVEL:
			olcp_process_low_level(request, size);
			break;
		default:
			olcp_build_error_report(OLCP_RC_BASIC, OLCP_BSER_INVALID_CLASS);
	}
}

