#include <stddef.h>
#include <stdint.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>

#include <init.h>
#include <output.h>
#include <scheduler.h>
#include <lights.h>
#include <hardware/generic.h>
#include <hardware/ioport.h>
#include <hardware/stled.h>
#include <hardware/usb.h>
#include <hardware/channels.h>
#include <hardware/powermon.h>

/* The list of functions that should be executed in time of firmware initialization */
static const InitFunc init_func_list[] = { generic_init, ioport_init, stled_init,
		scheduler_init, output_init, channels_init, powermon_init, usb_init, lights_init, NULL };

/* Firmware entry point */
int main() {

	uint8_t init_func_number;

	for (init_func_number = 0; init_func_list[init_func_number] != NULL; init_func_number++)
		init_func_list[init_func_number]();

	//output_status_and_halt(0x09);
	output_status(SC_NORMAL);

	//stled_set_state(true);

	/* Forever and ever */
	while (1) {
		sleep();
	}
	return 0;
}
