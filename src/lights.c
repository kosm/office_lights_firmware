/*
 * lights.c
 *
 *  Created on: 13.03.2011
 *      Author: kosm
 */

#include <lights.h>
#include <hardware/channels.h>

#define LIGHTS_TASK_PERIOD

static volatile bool initialized = false;

static

void lights_init() {
	if (initialized) return;


	initialized = true;
}
