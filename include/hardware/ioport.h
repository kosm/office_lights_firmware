/*
 * ioport.h
 *
 *  Created on: 09.01.2011
 *      Author: kosm
 */

#ifndef IOPORT_H_
#define IOPORT_H_

#include <common.h>

typedef enum { A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6, H = 7 } PortNumber;
typedef enum { PD_INPUT = 0, PD_OUTPUT = 1 } PinDirection;

typedef struct {
	PortNumber port_number;
	uint8_t pin_number;
} IOPortPin;

typedef struct {
	IOPortPin port_pin;
	PinDirection pin_direction;
	bool initial_state;
} IOPortPinConfig;

void ioport_init();
void ioport_configure_pin(const IOPortPinConfig *pin_config);
void ioport_configure_pins(const IOPortPinConfig pin_configs[], uint8_t length);
void ioport_set_pin(const IOPortPin *pin, bool value);
bool ioport_get_pin(const IOPortPin *pin);

#endif /* IOPORT_H_ */
