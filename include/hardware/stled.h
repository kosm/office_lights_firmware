/*
 * stled.h
 *
 *  Created on: 10.01.2011
 *      Author: kosm
 */

#ifndef STLED_H_
#define STLED_H_

#include <common.h>

void stled_init();
void stled_set_state(bool state);
bool stled_get_state();
void stled_toggle();

#endif /* STLED_H_ */
