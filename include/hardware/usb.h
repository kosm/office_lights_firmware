#ifndef USB_H_
#define USB_H_

/* Initializes the USB interface */
void usb_init();
/* The function continuously processing USB events */
void usb_routine(void *argument);

#endif
