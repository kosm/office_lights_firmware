/*
 * generic.h
 *
 *  Created on: 05.01.2011
 *      Author: kosm
 */

#ifndef GENERIC_H_
#define GENERIC_H_

#include <stdint.h>

void generic_init();
void sleep();
void delay(uint16_t interval);

#endif /* GENERIC_H_ */
