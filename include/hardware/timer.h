/*
 * timer.h
 *
 *  Created on: 10.02.2011
 *      Author: kosm
 */

#ifndef TIMER_H_
#define TIMER_H_

typedef void (* CounterCompareHandler)();

void timer_set_compare_handler(CounterCompareHandler handler);
void timer_init();

#endif /* TIMER_H_ */
