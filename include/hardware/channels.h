/*
 * channels.h
 *
 *  Created on: 27.02.2011
 *      Author: kosm
 */

#ifndef CHANNELS_H_
#define CHANNELS_H_

#include <stdbool.h>
#include <stdint.h>

#define CHANNEL_NUMBER 7

void channels_brightness_routine(void *argument);
void channels_blinking_routine(void *argument);
void channels_init();
void channels_set_state(uint8_t channel_num, bool state);
void channels_set_state_all(bool state);
void channels_set_blinking(uint8_t channel_num, bool blinking);
void channels_set_blinking_all(bool blinking);
void channels_set_brightness(uint8_t channel_num, uint8_t brightness);
void channels_set_brightness_all(uint8_t brightness);
void channels_set_blinking_period(uint8_t channel_num, uint8_t period);
void channels_set_blinking_period_all(uint8_t period);

#endif /* CHANNELS_H_ */
