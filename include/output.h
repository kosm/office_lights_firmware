/*
 * debug.h
 *
 *  Created on: 04.01.2011
 *      Author: kosm
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdbool.h>
#include <stdint.h>

/* Status codes */
#define SC_NONE				0xFF
#define SC_NORMAL			0x00
#define SC_EXTRA_TASK		0x01
#define SC_INVALID_VALUE	0x02
#define SC_NO_OUTPUT_POWER	0x03

/* A script describing a status indication scenario */
typedef struct {
	uint8_t *delays;
	uint8_t length;
	bool initial_state;
} StatusIndicationScript;

typedef enum { OUTPUT_ERROR, OUTPUT_INFO, OUTPUT_DEBUG } OutputLevel;

//void output_print(OutputLevel output_level, const char *format, ...);
void output_status_and_halt(uint8_t status_code);
void output_status(uint8_t status_code);
void output_indication_routine(void *argument);
void output_indicate_status(const StatusIndicationScript *indication_script);
void output_init();

#endif /* DEBUG_H_ */
