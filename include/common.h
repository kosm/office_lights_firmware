/*
 * common.h
 *
 *  Created on: 04.01.2011
 *      Author: kosm
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdint.h>
#include <stdbool.h>
#include <avr/pgmspace.h>

#define UNISTR(str) \
	L ## #str

#define PUNISTR(str) \
	PSTR(UNISTR(str))

#endif /* COMMON_H_ */
