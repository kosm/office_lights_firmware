/*
 * olcp_ll.h
 *
 *  Created on: 28.02.2011
 *      Author: kosm
 */

#ifndef OLCP_LL_H_
#define OLCP_LL_H_

#include <stdint.h>

uint8_t *olcp_get_response_buffer(uint8_t *buffer_size);
void olcp_process_request(const void *request, uint8_t size);

#endif /* OLCP_H_ */
