/*
 * scheduler.h
 *
 *  Created on: 13.02.2011
 *      Author: kosm
 */

#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include <stdint.h>

typedef void (* TaskRoutine)(void *argument);

typedef struct {
	TaskRoutine routine;
	void *argument;
	uint32_t period;
} Task;

void scheduler_add_task(const Task *task);
void scheduler_remove_task(const Task *task);
void scheduler_init();

#endif /* SCHEDULER_H_ */
